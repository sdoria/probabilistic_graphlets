# Probabilistic_Graphlets

This repository contains Python and C++ scripts as well as datasets for 
"Probabilistic Graphlets: A New Approach to Capture Biological Meaning from 
Probabilistic Molecular Networks." by Sergio Doria-Belenguer, 
Markus K. Youssef, René Böttcher, Noël Malod-Dognin and Nataša Pržulj.

*Corresponding author*: Prof.N.Przulj,
*email*: natasha@bsc.es

- Probabilistic Orbit Counter:

The probabilistic orbit counter can be easly applied using the python code
"Main_Orbit_Count.py". However, this code can be also run from the terminal as set
out in "Script_Parallel_Nodes_Count.py".